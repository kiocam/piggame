/*
GAME RULES:

- The game has 2 players, playing in rounds
- In each turn, a player rolls a dice as many times as he whishes. Each result get added to his ROUND score
- BUT, if the player rolls a 1, all his ROUND score gets lost. After that, it's the next player's turn
- The player can choose to 'Hold', which means that his ROUND score gets added to his GLBAL score. After that, it's the next player's turn
- The first player to reach 100 points on GLOBAL score wins the game

*/


var $, scores, roundScore, activePlayer;

$ = document.querySelector.bind(document);

scores = [0, 0];
roundScore = 0;
activePlayer = 0;


$('#score-0').textContent = '0';
$('#score-1').textContent = '0';
$('#current-0').textContent = '0';
$('#current-1').textContent = '0';

$('.dice').style.display = 'none';


$('.btn-roll').addEventListener('click', function () {
    // generate a random number
    var dice = Math.floor(Math.random() * 6) + 1;

    // display the result
    var diceDom = $('.dice');
    diceDom.style.display = 'block';
    diceDom.src = "assets/dice-" + dice + '.png';

    //update the round score if the rolled number was Not a 1
    if (dice !== 1) {
        //add score
        roundScore += dice;
        $('#current-' + activePlayer).textContent = roundScore;
        $('.dice').style.display = 'none';
        $('.player-' + activePlayer + '-panel').classList.add('winner');
        $('.player-' + activePlayer + '-panel').classList.remove('active');
    } else {
        //next player
        nextPlayer();
    }

});




$('.btn-hold').addEventListener('click', function () {
    //Add current score to global score
    scores[activePlayer] += roundScore;

    //update the UI
    $('#score-' + activePlayer).textContent = scores[activePlayer]
    // Check if player  won the game
    if (scores[activePlayer] >= 20) {
        $('#name-' + activePlayer).textContent = 'Winner!';
    }
    //Next PLayer
    nextPlayer();
});

function nextPlayer() {
    activePlayer === 0 ? activePlayer = 1 : activePlayer = 0;
    roundScore = 0;

    $('#current-0').textContent = '0';
    $('#current-1').textContent = '0';

    $('.player-0-panel').classList.toggle('active');
    $('.player-1-panel').classList.toggle('active');

    $('.dice').style.display = 'none';
    
}

$('.btn-new').addEventListener('click', function (){
    
})
